# Task 1-2-3

Yet another task tool.  This one offers a simple focus, 1 main task, 2 minor tasks, and 3 optionals.

## Development
    $ composer update
    $ npm update
    $ php artisan serve
    $ npm run dev

Browse to [http://localhost:8000](http://localhost:8000).